from django.urls import path

from .views import ItemListView, AddToCartView, OrderDetailView, ItemDetailView

urlpatterns = [
    path('products/', ItemListView.as_view(), name='product_list'),
    path('products/<pk>/', ItemDetailView.as_view(), name='product_detail'),
    path('add-to-cart/', AddToCartView.as_view(), name='add-to-cart'),
    path('order-summary/', OrderDetailView.as_view(), name='order-summary'),
    path('addresses/<pk>/update/',
         AddressUpdateView.as_view(), name='address-update'),
    path('addresses/<pk>/delete/',
         AddressDeleteView.as_view(), name='address-delete'),
]
